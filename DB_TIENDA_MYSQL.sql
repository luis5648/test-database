-- script para crear base de datos de una tienda en mysql

drop database tienda ;
CREATE database tienda;
use tienda;

create table tienda.categorias(
	ID_CATEGORIA int auto_increment primary key not null,
	NOMBRE_CATEGORIA VARCHAR(20)

);


create table tienda.proveedores(
	ID_PROVEEDOR int auto_increment primary key not null,
	NOMBRE_PROVEEDOR VARCHAR(20),
	TELEFONO VARCHAR(14),
	DIRECCION VARCHAR(50)


);

create table tienda.usuarios(
	ID_USUARIO int auto_increment primary key not NULL,
	NOMBRE_USUARIO VARCHAR(15),
	APELLIDO_MATERNO VARCHAR(15),
	APELLIDO_PATERNO VARCHAR(15),
	CORREO VARCHAR(25),
	PASS VARCHAR(10)

);

create table tienda.productos(
	ID_PRODUCTO int auto_increment primary key not null,
	NOMBRE_PRODUCTO VARCHAR(50),
	PRECIO INTEGER,
	PROVEEDOR INTEGER,
	CATEGORIA INTEGER,
	
	foreign key (PROVEEDOR) references tienda.proveedores(ID_PROVEEDOR)on delete cascade on update CASCADE,
	foreign key (CATEGORIA) references tienda.categorias(ID_CATEGORIA)on delete cascade on update CASCADE
	
);

create table tienda.ventas(
	ID_VENTA int auto_increment primary key not null,
	USUARIO INTEGER,
	PRODUCTO INTEGER,
	FECHA DATE,
	
	
	foreign key (USUARIO) references tienda.usuarios(ID_USUARIO) on delete cascade on update CASCADE,
	foreign key (PRODUCTO) references tienda.productos(ID_PRODUCTO) on delete cascade on update CASCADE
	

);


INSERT INTO usuarios (nombre_usuario,apellido_materno,apellido_paterno,correo,pass) VALUES 
('LUIS GERARDO','ROMAN','MARIN','romnluis@yahoo.com','newborn')
,('oscar','roman','marin','oscar@gmail.com','12345');

INSERT INTO categorias (nombre_categoria) VALUES 
('farmacia')
,('linea blanca')
,('alimentos y bebidas')
,('cremeria')
,('vinos y licores')
;
INSERT INTO proveedores (nombre_proveedor,telefono,direccion) VALUES 
('pepsico','3321145877','sample address 1')
,('bimbo','1145598866','sa2')
,('cervecería modelo','5577896331','sa3')
,('bebidas chidas','5566977411','sa4')
,('electronicos-sa','5544772231','sa5')
;

INSERT INTO productos (nombre_producto,precio,proveedor,categoria) VALUES 
('tonayan ',20,4,5)
,('pan de caja blanco',32,2,3)
,('horno de micro-ondas',900,5,2)
,('queso manchego kg',100,2,4)
,('pepsi 1lt',12,1,3)
,('medias noches',25,2,3)
,('salchicha de pavo kg',85,2,4)
,('refresco 7up 2lts',31,1,3)
;
INSERT INTO ventas (usuario,producto,fecha) VALUES 
(1,1,'2020-10-10')
,(1,2,'2020-10-10')
,(1,1,'2020-10-10')
,(2,1,'2020-10-10')
,(2,1,'2020-09-09')
;


update tienda.usuarios set nombre_usuario='luis gerardo' where id_usuario =1;
select * from tienda.usuarios;

ALTER TABLE tienda.productos ALTER COLUMN nombre_producto TYPE varchar(30) USING nombre_producto::varchar;

alter table tienda.usuarios
	rename column correo to email;




-- joins de ventas

select * from tienda.ventas;

select nombre_usuario, nombre_producto as producto_vendido, nombre_proveedor as proveedor_del_producto, fecha from tienda.ventas v 
	inner join 
		tienda.usuarios u on u.id_usuario = v.usuario
	inner join
		tienda.productos p on p.id_producto = v.producto 
	inner join 
		tienda.proveedores p2 on p2.id_proveedor = p.id_producto;
	
-- consultas para pruebas de obtención de información

	select * from tienda.ventas v 
	
-- consulta para saber que usuario tiene el mayor numero de ventas:
	select count(usuario) as total_ventas, nombre_usuario from tienda.ventas v 
	inner join 
		tienda.usuarios u  on v.usuario = u.id_usuario 
	group by u.nombre_usuario order by total_ventas desc limit 1;
	
insert into tienda.productos (nombre_producto ,precio ,proveedor ,categoria ) values('refresco 7up 3lts', 31,1,3);

select * from tienda.productos p  

update tienda.productos set nombre_producto = 'refresco 7up 2lts' where id_producto = 8;


select count(*)as usuarios_ventas from tienda.ventas v where usuario =2;

select min(nombre_usuario) from tienda.ventas v 
inner join tienda.usuarios u on v.usuario = u.id_usuario ;

